
Electroencephalography (EEG) signals provide a representation of the brain's activity patterns and have been recently exploited for user identification and authentication due to their uniqueness and their robustness to interception and artificial replication. Nevertheless, such signals are commonly affected by the individual's emotional state. In this work, we examine the use of images as stimulus for acquiring EEG signals and study whether the use of images that evoke similar emotional responses leads to higher identification accuracy compared to images that evoke different emotional responses. Results show that identification accuracy increases when the system is trained with EEG recordings that refer to similar emotional states as the EEG recordings that are used for identification, demonstrating an up to $5.3\%$ increase on identification accuracy compared to when recordings referring to different emotional states are used. Furthermore, this improvement holds independently of the features and classification algorithms employed.
%
\section{Introduction}
\label{ch6:sec:intro}
It is well documented that images can evoke a wide range of emotions \cite{kurdi2017introducing}, such as pleasure, frustration, happiness, disgust, sadness, etc. Relations between the stimulus and evoked emotional responses have been extensively studied by psychologists, while affective computing approaches have employed physiological signals, such as electroencephalography (EEG) and electrocardiography (\acrshort{ecg}), for algorithmic affect recognition \cite{arnau2017}. EEG signals provide a representation of the brain's activity patterns and thus are significantly affected by an individual's emotional state during recording \cite{DREAMER}. Apart from affect recognition, EEG signals have a wide range of applications, including, more recently, biometrics. EEG-based biometrics provide some advantages over traditional biometrics approaches, such as robustness against artificial reproduction of the original signals and against capturing from a distance \cite{delpozo14}. Nevertheless, EEG-based biometrics pose new challenges, such as the problem of permanence of the recorded signals, i.e. finding patterns that are not heavily affected by template ageing, and the problem of defining suitable signal acquisition protocols in relation to the stimuli and the devices used \cite{effaff}. 

Many studies have been recently published in the field of EEG-based biometrics, and \cite{campisi2014brain} and \cite{SurveyIET} provided an extensive review on their prospects and theoretical background. The EEG signal acquisition protocol is critical to the effectiveness of an EEG-based biometrics system, since the identity content within the brain signal can be affected by the task that the subjects are engaged into. Subjects are usually requested to perform specific tasks or are exposed to predefined stimuli (e.g. images or video). Ruiz-Blondet et al. \cite{CEREBRE} and Maiorana and Campisi \cite{maiorana2018longitudinal} examined the use of various tasks in order to achieve brain stimulation, typically resting state, audiovisual stimuli (sensory activities), or cognitive tasks, as commonly proposed in the literature \cite{EEGsurvey}. Imagined speech \cite{Brigham10} and custom tasks \cite{chuang2013}, as well as the use of event-related potentials (ERPs) \cite{armstrong2015} have also been employed and led to increased subject identification accuracy and stability over time (enhanced permanence). \par

Despite the various research works on EEG-based biometrics, the effect of the user's emotional state during EEG signal acquisition has received little attention. Emotion recognition research \cite{arnau2017,arnau2016method} provides evidence that the subject's affective state is reflected within the EEG signal, a fact that can be possibly exploited within the subject identification context by linking emotional responses from specific tasks to specific individuals. During interaction with an EEG-based biometrics system, the users emotional state is unknown and disregarded, and is expected to vary between separate interactions with the system. As a result, varying emotional states can potentially act as noise within the acquired EEG signal, thus obstructing pattern matching. In \cite{effaff}, the authors studied the effect of varying emotional responses elicited by video stimuli in the context of EEG-based subject identification. Results showed that regardless the EEG features used, the machine learning method, and the time between signal acquisition, the use of EEG recordings associated with similar emotional states provided higher identification accuracy than the use of EEG signals associated with different emotional states.

Regardless the theoretical interest of the research reported in \cite{effaff}, the exposure time required for video stimuli has a significant influence on the applicability of the method in realistic contexts. In order to overcome this major limitation, we have extended this previous research by studying whether the effect reported also holds when images are used as the stimulus. To this end, two publicly available datasets containing images and their associated emotional responses in terms of valence and arousal (Russell's valence-arousal scale \cite{RUSSELL1977273}), were used as stimuli for the acquisition of EEG recordings. The participants of the study provided their self-assessed emotional state after each stimulus and the acquired recordings were then used to create various supervised classification models for the task of subject identification. Results using different EEG features and classification methods showed that EEG recordings referring to similar emotional states provided consistently higher identification accuracy than recordings referring to different emotional states, independently of the features, classification method, and signal acquisition session. Furthermore, the use in this work of a portable low-cost off-the-shelf EEG device \cite{Badcock:2013:EPOC}, in contrast to the medical-grade EEG devices used in \cite{effaff}, shows that our finding is also valid for EEG devices with a lower spatial resolution (less electrodes). 

The rest of this paper is organised in three sections. The methodology followed is described in Section \ref{ch6:sec:method}, while Section \ref{sec:results} provides the experimental procedure and acquired results. Finally, conclusions are drawn in Section \ref{ch6:sec:conclusion}.



\section{Methodology}
\label{ch6:sec:method}

\subsection{Datasets}
\label{ch6:sec:method:datasets}
Image stimuli with strong emotional content were used in this study in order to elicit affect reactions to the participants and record EEG data. The stimuli were obtained from two publicly available image datasets, i.e. the Geneva Affective Picture Dataset (GAPED) \cite{dan2011geneva} and the Open Affective Standardised Image Set (OASIS) \cite{kurdi2017introducing}: \par

GAPED contains 730 different images with a resolution of $640\times 479$ pixels encoded in \acrshort{jpeg} format. All the images are rated in terms of arousal and valence within the range $\left[0,\ 100\right]$. The dataset's images belong to the following categories: \textit{Snakes, Spiders, Human concerns (depicting scenes violating human rights), Animal mistreatments (picturing animal mistreatment scenes), Neutral, and Positive}.\par

OASIS contains 900 different images with a resolution of $500\times 400$ pixels encoded in \acrshort{jpeg} format. The images are rated in terms of arousal and valence within the range $\left[ 1,\ 7\right]$. Furthermore, it contains the average responses for each image divided by gender. The OASIS dataset contains images belonging to one of the following four mutually exclusive categories: \textit{Animals, Objects, People, and Scenes}. It must be noted that some of the images contain explicit adult content, leading to the exclusion of image $\#I537$ from this study due to very explicit content.\par

The two datasets contained a total of 1,630 images from which 48 were selected according to their labels, in order to obtain a representative set of images with an intense emotional content. To this end, the valence-arousal labels of each dataset were first normalised to the range $\left[-1,\ 1\right]$. Then, the resulting valence-arousal space was divided in 12 equal regions, as shown in \figurename~\ref{ch6:fig:selected_images}. Finally, we selected the 4 images from each region whose (Valence , Arousal) coordinates were farthest from the absolute neutral emotion $\left( 0,0\right)$, as these contained the most intense emotional content within that region.\par 


\begin{figure}
 \centering
\resizebox {\columnwidth} {!} {
    \input{img/chapter_6/selected_pictures_img.tikz}
}
\caption{Images of the GAPED and OASIS datasets in Valence-Arousal space. Marked images were selected for this study.}
\label{ch6:fig:selected_images}
\end{figure}

\subsection{Data acquisition and experimental protocol}

26 healthy subjects, aged between 23 and 55 years old ($\mu_{age}=31.9$), were recruited as volunteers for data acquisition. The experiment took place inside a quiet room with ambient light and no physical supervision, in order to not alter the response of the participants and not introduce any artefacts related to stress or distractions. Before starting the experiment, the experimental procedure and the used valence-arousal scale were explained, and participants were then asked to fill a consent form indicating that they agree to participate in the study and to the viewing of images that may depict strong emotional content.\par

Three sessions were recorded for each participant, each of them spaced 7 days apart, and the average duration of each session was approximately 7 \textit{min}. Out of the four images selected per region (Section \ref{ch6:sec:method:datasets}), one was randomly selected to be displayed in all the sessions as baseline, and out of the remaining three, one was assigned to each of the sessions, generating as a result three sets of 24 images (12 repeated and 12 unique images per set). During each session, each image was presented to the participants for 5 \textit{sec} and then participants were asked to report the felt emotion using Self Assessment Manikins (\acrshort{sam}) \cite{SAM in a 9-point scale (1-9). After the self assessment, participants were asked to perform a simple mathematical operation and report the result, in order to reduce any effects of the emotional stimulus to their emotional state. The session was divided in two parts: 1) In the first part, the 12 repeated images that would be the same for all three sessions were displayed, but presented in different order for each of the sessions in order to avoid any presentation order effects. 2) In the second part, the 12 unique images were displayed. During the whole session, EEG was recorded using the 14-channel Emotiv EPOC+\textsuperscript{\textregistered} wireless EEG device with a sampling rate of 256Hz. Furthermore, timestamps with millisecond precision were used to synchronise the acquired EEG signals with the image stimuli exposure.



\subsection{Data preparation and feature extraction}
 In order to remove artefacts, such as muscle movement, jaw clenching, or eye blinking, the EEGLAB toolbox \cite{delorme2004eeglab} has been used to pre-process the EEG signals by applying the PREP pipeline for EEG data pre-processing as described in \cite{bigdely2015prep}. The PREP pipeline consists of the following steps: (1) Remove line-noise with filtering, (2) Reference the signal relative to an estimate of the ``true" average reference, and (3) Detect and interpolate bad channels, relative to the reference. \par

Then, in order to create a machine learning model for subject identification from EEG signals, various features were extracted from the pre-processed EEG recordings, namely  the Mel Frequency Cepstral Coefficients (MFCC), Power Spectral Density (PSD), and Autoregression Reflection Coefficients (ARRC):


 \begin{figure}
 \centering
	\resizebox {\columnwidth} {!} {
        \input{img/chapter_6/correlation_valence.tex}
    }
    \label{ch6:fig:corr:val}
    \caption{Original emotional ratings from the datasets vs acquired ratings for Valence,}
 \end{figure}
 
  \begin{figure}
   \centering
	\resizebox {\columnwidth} {!} {
        \input{img/chapter_6/correlation_arousal.tex}
    }
    \label{ch6:fig:corr:aro}
    \caption{Original emotional ratings from the datasets vs acquired ratings for Arousal}
 \end{figure}

\subsubsection{Power Spectral Density (PSD)}
PSD-based features, have been commonly used for identifying emotions from EEG signals \cite{arnau2017,DREAMER}. In this work, PSD features are computed as described in \cite{delpozo2018evidence}: First, the PSD is computed using Welch's algorithm on each of the channels. For each of the channels, the PSD is computed using a Hamming window of 2 \textit{sec} (512 samples) with an overlapping of 75$\%$ (384 samples) and the FFT is produced over each of these windows, and averaged across the time. Finally the feature vector is created by concatenating the values of the resulting PSDs over the frequency band $\left[ 1 - 40 \right]$ Hz. This process results into a total of 38 features per channel, leading to a total of 532 features (38 features $\times$ 14 channels).

%%% REF 8461852 is a paper published in ICASSP 2018 that uses MFCCs, in speech recognition, I think they will appreciate the citation
\subsubsection{Mel Frequency Cepstral Coefficients (MFCCs)}
MFCCs are a parametric representation of the Fourier Spectrum and have been widely used in speech recognition \cite{ittichaichareon2012speech,8461852} and more recently applied to EEG-based subject identification \cite{piciucco2017steady,nguyen2012proposed}, achieving relatively high accuracies. In this work we use the well-known MFCC computation using \acrshort{htk}-like filterbanks and Discrete Cosine Transform. Following \cite{piciucco2017steady}, MFCC features have been computed using 18 filterbanks, producing a total 12 cepstral coefficients per channel. The final feature vector is created by concatenating the cepstral coefficients of each of the channels, leading to a total of 168 features (12 coefficients $\times$ 14 channels).


\subsubsection{Autoregression Reflection Coefficients (ARRCs)}
ARRCs indicate the time dependence of a signal $x$ between $x(t)$ and $x(t-k)$. ARRCs have been widely used for classification of EEG signals \cite{rahman2018efficient} and more recently applied to EEG-based biometrics \cite{piciucco2017steady,hine2017resting,Maiorana16}. Based on the autoregressive (AR) or all-pole model of the EEG spectrum, an EEG signal can be characterised as an output of a causal stable linear time-invariant stationary AR($P$-th order) system. Then, by using Yule-Walker's equations \cite{kay1988modern} the parameters of the AR are estimated. In this study, the reflection coefficients have been computed using MATLAB's implementation and the ARRCs have been obtained by estimating an AR model of order 10 for each channel separately. The final feature vector has been created by concatenating the 10 computed reflection coefficients for each channel, leading to a total of 140 features (10 coefficients $\times$ 14 channels).

\subsection{Sample creation and classification}
The acquired feature vectors were then labelled according to the participant's ID and the emotional response in terms of valence and arousal. As commonly performed in emotion recognition studies \cite{arnau2017,DREAMER,effaff,delpozo2018evidence}, valence and arousal values where thresholded to negative/positive and high/low respectively.  Due to the use of the \acrshort{sam} manikins with the 9-point scale, each dimension has a total of 9 possible answers, (5 manikins and 4 middle points). Since the amount of possible answers is odd, and in order to put the threshold value in the middle of the scale and avoid class imbalances, samples referring to neutral answers (middle answer) were discarded. From the remaining 8 possible answers, the lower 4 were considered as negative/low and the higher 4 were considered as positive/high. \par

Supervised classification experiments were then conducted in order to create various models for EEG-based subject identification. The examined classification algorithms included the \textit{k}-Nearest Neighbour (\textit{k}NN) method for $k=3,5,7$ and the Support Vector Machine (\acrshort{svm}) with Linear and Radial Basis (\acrshort{rbf}) kernel.





\section{Results}
\label{ch6:sec:results}

\subsection{Emotional ratings evaluation}
In  order  to  establish  the quality of the captured  data, the average emotional ratings reported by the participants of this study for each image were compared to the normalised emotional ratings for the same images available from the respective datasets. It is critical to establish whether  the  current  ratings  agree  with  the  already  available ratings since if the captured ratings differ significantly, then the findings  of  this  study  would  be  disputable  due  to  unsuitable data.  The  Pearson’s  Correlation  Coefficient  ($\rho$)  was  computed separately for each affective dimension,  resulting  to  a  very strong  correlation for valence  ($\rho=0.9753$) and a strong correlation for arousal ($\rho=0.84265$) between  the  ratings provided by the datasets and the average ratings acquired in this study. The strong correlation of the ratings is also evident in \figurename~\ref{ch6:fig:corr:val}, and \ref{ch6:fig:corr:}  where the average emotional ratings reported in this study are plotted against the ratings by the GAPED and OASIS datasets. \par



\begin{table}[tp]
\caption{Accuracy  for \\acrshort{sam}E~ and \DIFF~ for each session and setting}
\label{ch6:tab:results}
\centering
\renewcommand{\arraystretch}{1.1}
\setlength{\tabcolsep}{4pt}
\small
\begin{threeparttable}
\begin{tabular}{|l|l|l|l|l|l|l|}
\multicolumn{7}{c}{\textbf{Testing with SESSION 1}} \\
\hline
               Feature                             & Setting      & L\acrshort{svm}            & R\acrshort{svm}            & 3NN             & 5NN             & 7NN             \\ \hline \hline
\multicolumn{1}{|l|}{\multirow{2}{*}{MFCC}} & \acrshort{sam}E & \textbf{0.3490$^\dagger$} & \textbf{0.2446} &  \textbf{0.2649} & \textbf{0.2496} & \textbf{0.2599} \\ \cline{2-7} 
\multicolumn{1}{|l|}{}                      & DIFF & 0.3113          & 0.2282          & 0.2329          & 0.2232          & 0.2241          \\  \hline \hline
\multicolumn{1}{|l|}{\multirow{2}{*}{PSD}}  & \acrshort{sam}E & \textbf{0.1499} & \textbf{0.0604} &  \textbf{0.1123} & \textbf{0.1036} & \textbf{0.1041} \\ \cline{2-7} 
\multicolumn{1}{|l|}{}                      & DIFF & 0.1232          & 0.0303          &  0.0934          & 0.0812          & 0.0685          \\ \hline \hline
\multicolumn{1}{|l|}{\multirow{2}{*}{ARRC}} & \acrshort{sam}E & \textbf{0.1161} & \textbf{0.0845} &  \textbf{0.0801} & \textbf{0.0815} & \textbf{0.1025} \\ \cline{2-7} 
\multicolumn{1}{|l|}{}                      & DIFF & 0.0886          & 0.0457          &  0.0506          & 0.0394          & 0.0517          \\ \hline 

\multicolumn{7}{c}{} \\
\multicolumn{7}{c}{\textbf{Testing with SESSION 2}} \\

\hline
               Feature                             & Setting      & L\acrshort{svm}            & R\acrshort{svm}            & 3NN             & 5NN             & 7NN             \\ \hline \hline
\multicolumn{1}{|l|}{\multirow{2}{*}{MFCC}} & \acrshort{sam}E & \textbf{0.2664$^\dagger$} & \textbf{0.2463} &  \textbf{0.2293} & \textbf{0.2197} & \textbf{0.2108} \\ \cline{2-7} 
\multicolumn{1}{|l|}{}                      & DIFF & 0.2195                    & 0.1892          &  0.1965          & 0.1801          & 0.1656          \\ \hline \hline
\multicolumn{1}{|l|}{\multirow{2}{*}{PSD}}  & \acrshort{sam}E & \textbf{0.1348}           & \textbf{0.0655} &  \textbf{0.1665} & \textbf{0.1572} & \textbf{0.1525} \\ \cline{2-7} 
\multicolumn{1}{|l|}{}                      & DIFF & 0.1174                    & 0.0211          &  0.1442          & 0.1327          & 0.1323          \\ \hline \hline
\multicolumn{1}{|l|}{\multirow{2}{*}{ARRC}} & \acrshort{sam}E & \textbf{0.1460}           & \textbf{0.0876} &  \textbf{0.0848} & \textbf{0.0905} & \textbf{0.0990} \\ \cline{2-7} 
\multicolumn{1}{|l|}{}                      & DIFF & 0.1049                    & 0.0577          &  0.0696          & 0.0748          & 0.0856          \\ \hline


\multicolumn{7}{c}{} \\
\multicolumn{7}{c}{\textbf{Testing with SESSION 3}} \\

\hline
               Feature                             & Setting      & L\acrshort{svm}            & R\acrshort{svm}            & 3NN             & 5NN             & 7NN             \\ \hline \hline
\multicolumn{1}{|l|}{\multirow{2}{*}{MFCC}} & \acrshort{sam}E & \textbf{0.2919$^\dagger$} & \textbf{0.2683} &  \textbf{0.2566} & \textbf{0.2516} & \textbf{0.2326} \\ \cline{2-7} 
\multicolumn{1}{|l|}{}                      & DIFF & 0.2631          & 0.2410          &  0.2388          & 0.2171          & 0.1910          \\ \hline \hline
\multicolumn{1}{|l|}{\multirow{2}{*}{PSD}}  & \acrshort{sam}E & \textbf{0.1319} & \textbf{0.0568} &  \textbf{0.1881} & \textbf{0.1882} & \textbf{0.1829} \\ \cline{2-7} 
\multicolumn{1}{|l|}{}                      & DIFF & 0.1108          & 0.0174          &  0.1344          & 0.1473          & 0.1370          \\ \hline \hline
\multicolumn{1}{|l|}{\multirow{2}{*}{ARRC}} & \acrshort{sam}E & \textbf{0.1285} & \textbf{0.0777} &  \textbf{0.1056} & \textbf{0.1187} & \textbf{0.1166} \\ \cline{2-7} 
\multicolumn{1}{|l|}{}                      & DIFF & 0.1005          & 0.0389          &  0.0698          & 0.0968          & 0.1021          \\ \hline

\end{tabular}
\end{threeparttable}
\begin{tablenotes} 
\scriptsize  \textbf{Note}: Values in bold represent the highest value for each \\acrshort{sam}E/\DIFF~pair.\\ $\dagger$ represents the highest accuracy achieved for the session.
 \end{tablenotes}

\end{table}



\subsection{Classification Experiments}

Supervised multi-class classification experiments were conducted using a Leave-One-Session-Out cross validation scheme, where the samples from one session were used for testing and the samples from the other two sessions were used for training. The process was repeated for all three available sessions. This approach allowed us to take into account the template ageing effect of the EEG signals and remove the bias from training and testing using EEG samples acquired during the same session. Following this validation scheme, two experiments were conducted, namely \\acrshort{sam}E~ and \DIFF. \par

Under the \\acrshort{sam}E~experimental scenario, the classification models were trained and tested with samples referring to similar emotional states. As a result, after dividing each session's samples into four groups according to the valence-arousal ratings (negative valence (NV), positive valence (PV), low arousal (LA), high arousal (HA)), the Leave-One-Session-Out cross validation scheme was conducted separately for each group. The four obtained accuracy values were then averaged and reported as the accuracy for \\acrshort{sam}E~ under the examined settings. Contrary to \\acrshort{sam}E, the classification models for \DIFF~ were trained and tested with samples referring to different emotional states, leading to four training and test groups: NV vs PV, PV vs NV, LA vs HA, HA vs LA, and the Leave-One-Session-Out cross validation scheme was again conducted separately for each group. The four obtained accuracy values were then averaged and reported as the accuracy for \DIFF. This experimental setup allowed us to study both the effect of emotion on the subject identification task, as well as the permanence of this effect, and its evolution over the studied period of time.

The aforementioned experiments were conducted for the PSD, MFCC and ARRC features using five different classification approaches, i.e. Linear \acrshort{svm} (L\acrshort{svm}), Radial Basis \acrshort{svm} (R\acrshort{svm}), 3 Nearest Neighbours (3NN), 5 Nearest Neighbours (5NN), and 7 Nearest Neighbours (7NN). The obtained results in terms of classification accuracy are reported in Table \ref{ch6:tab:results} for when using the samples of the first, the second, and the third session respectively as the test set. It can be observed that generally, the linear \acrshort{svm} achieves better accuracy regardless of the features used, although not for all the experimental settings. Nevertheless, the use of the linear \acrshort{svm} along with the MFCC features provides the highest classification accuracy for all the three sessions, achieving an average accuracy of $0.3024$ across the three sessions for \\acrshort{sam}E, and an average accuracy of $0.2646$ for \DIFF. \par

However, the difference in performance between the experiments \\acrshort{sam}E~and \DIFF~is easily noticeable from Table \ref{ch6:tab:results}, as well as from the box plots in \figurename~\ref{ch6:fig:barplotdiff}. It is evident that in all experimental settings, identification accuracy is higher when using EEG samples referring to similar emotional states (\\acrshort{sam}E) than when using EEG samples referring to different emotional states (\DIFF), regardless the features and classification method used or the session. On average, training with samples referring to similar emotional states provides an average boost of $\sim 3\%$ in accuracy compared to training and testing with samples referring to different emotional states. \figurename~\ref{ch6:fig:boxplot} shows the distribution of accuracy for all the three sessions, for the best performing classifier. As it can be seen, in all cases, the median accuracy is higher for the \\acrshort{sam}E~ experiment, consistently and independently of the features used. The difference in the highest accuracies achieved for \\acrshort{sam}E~ and \DIFF~ are illustrated in \figurename~\ref{ch6:fig:barplotdiff}, where it can be observed that in the worst case, \\acrshort{sam}E~ overperforms \DIFF~ by $2\%$ and in the best case by more than $5\%$. \par

This increase in performance can be exploited to improve EEG-based biometric systems by taking into consideration the emotional state of the users. Furthermore, the obtained results are consistent with those reported in \cite{effaff} for when video stimuli and medical grade EGG devices were used, thus extending the findings of \cite{effaff} to image stimuli and low-cost EEG devices with lower spatial resolution and highlighting the importance of knowing the affective state of the users in order to provide better EEG-based biometric systems.





\begin{figure}[t]
\centering
\begin{tikzpicture}
  \begin{axis}
    [
    cycle list={{black},{dashed,gray}},
    height = 5.5cm,
    width = 8.0cm,
    xtick={1.5,3.5,5.5},
    ylabel={Accuracy},
    ymax={0.5},
    ymin=0.0,
    xticklabels={$MFCC$, $PSD$,$ARRC$},
    label style={font=\normalsize},
    %xticklabel style = {rotate=0,anchor=east},
    boxplot/draw direction=y,
    legend entries={\\acrshort{sam}E,\DIFF},
    legend pos=north east,
    ]
    \addlegendimage{black,no markers}
    \addlegendimage{gray,no markers,dashed}
    \addplot+[
    boxplot prepared={
      lower whisker=0.2260,
      lower quartile=0.2743,
      median=0.29,
      upper quartile=0.335,
      upper whisker=0.4260,
      %every box/.style={draw=black},
    },
    ] coordinates {};
    \addplot+[
    boxplot prepared={
      lower whisker=0.1710,
      lower quartile=0.2102,
      median=0.2752,
      upper quartile=0.3096,
      upper whisker=0.3532
    },
    ] coordinates {};
    \addplot+[
    boxplot prepared={
      lower whisker=0.0844,
      lower quartile=0.1297,
      median=0.1561,
      upper quartile=0.1825,
      upper whisker=0.2238
    },
    ] coordinates {};
    \addplot+[
    boxplot prepared={
      lower whisker=0.0628,
      lower quartile=0.1058,
      median=0.1208,
      upper quartile=0.1559,
      upper whisker=0.1681
    },
    ] coordinates {};
    \addplot+[
    boxplot prepared={
      lower whisker=0.0575,
      lower quartile=0.1026,
      median=0.1268,
      upper quartile=0.1660,
      upper whisker=0.1878
    },
    ] coordinates {};
    \addplot+[
    boxplot prepared={
      lower whisker=0.0558,
      lower quartile=0.0837,
      median=0.0911,
      upper quartile=0.1120,
      upper whisker=0.1593
    },
    ] coordinates {};
  \end{axis}
\end{tikzpicture}
\caption{Accuracy values achieved for \\acrshort{sam}E~and \DIFF. The classifier that achieved the highest accuracy is depicted for each feature type, i.e. L\acrshort{svm} for MFCC, 3NN for PSD, and L\acrshort{svm} for ARRC.}
\label{ch6:fig:boxplot}
\end{figure}



\begin{figure}[t]
\centering
\begin{tikzpicture}
\begin{axis}[
    ybar,
    height = 5.0cm,
    width = 8.0cm,
    legend pos=north east,
    %legend style={legend columns=-1},
    legend style={at={(0.5,1.2)},
      anchor=north,legend columns=-1},
      scaled ticks=false,
    ylabel={$ACC_{\acrshort{sam}E}-ACC_{DIFF}$},
    y tick label style={
    	/pgf/number format/.cd,
        fixed,
        fixed zerofill,
        precision=2,
    	/tikz/.cd
    	},
    ymax = 0.06,
    symbolic x coords={MFCC,PSD,ARRC},
    xtick=data,
    %nodes near coords,
    nodes near coords align={vertical},
    enlarge x limits = 0.3
    ]
\addplot [black!95!black,fill=black!95!white] coordinates {(MFCC,0.0377) (PSD,0.0190) (ARRC,0.0275)};
\addplot [black!55!black,fill=black!55!white] coordinates {(MFCC,0.0469) (PSD,0.0223) (ARRC,0.0411)};
\addplot [black!15!black,fill=black!15!white] coordinates {(MFCC,0.0288) (PSD,0.0537) (ARRC,0.0280)};
\legend{SESSION 1,SESSION 2,SESSION 3}
\end{axis}
\end{tikzpicture}
\caption{Difference in highest accuracy for each feature between the \\acrshort{sam}E~ and \DIFF~ experiments.}
\label{ch6:fig:barplotdiff}
\end{figure}

% \acrshort{sam}E and DIFF. The results shown correspond to the best classifier for each feature L\acrshort{svm} for MFCC features, 3NN for PSD features, and Q\acrshort{svm} for ARRC features

\section{Conclusion}
\label{ch6:sec:conclusion}
In this work we examined the effect of the emotional state of the subject in the context of EEG-based biometrics using images as stimulus for acquiring the EEG signals. Results showed a difference in performance when taking into account the emotional state of the subjects and showed that using EEG recordings referring to similar emotional states for training the system and for identifying the individual, leads to higher identification accuracy than when using EEG recordings referring to different emotional states. The results are consistent with a similar previous study that utilised video stimuli and medical-grade EEG devices, showing that this finding can be extended to image stimulus and low-cost EEG devices, and that it is independent from the features, classification algorithm and EEG device used. Consequently, EEG-based biometric systems would be qualitatively improved if this effect is taken into consideration for creating emotion-based strategies in their design. Future work will include the design of potential strategies for incorporating the emotional state of the individual into the EEG-biometrics pipeline and the study of other types of stimulus in relation to the emotional state.
\filbreak
\section{Acknowledgments}
This work has been partially funded by the Spanish Ministry of Economy and Competitiveness through project TIN2014-59641-C2-1-P and by the University of the West Scotland Vice Principal's funding.

\vfill\pagebreak


